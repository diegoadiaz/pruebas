# Generated by Django 4.0.3 on 2022-04-20 16:10

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='crearEstudiante',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=200)),
                ('apellido', models.CharField(max_length=200)),
                ('semestre', models.CharField(max_length=200)),
                ('email', models.CharField(max_length=200)),
                ('documento', models.CharField(max_length=200)),
            ],
        ),
    ]
