
from pickle import TRUE
from django.shortcuts import render
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from app.models import Decano, Facultad, Estudiante, EstadoVotacion, TipoVotacion, Votacion,Candidato, Voto
from django.http import HttpResponse
from django.contrib.auth import authenticate, login, logout
 
def index(request):
    return HttpResponse("Hola, mundo")


def login_view(request):
    return render(request, 'app/login.html')

def entrar(request):
    # Obtiene los datos del formulario de autenticación
    username = request.POST['username']
    password = request.POST['password']
    # Obtiene el usuario
    usuario = authenticate(username=username, password=password)

    # Verifica si el usuario existe en la base de datos 
    if usuario is not None:
        # Inicia la sesión del usuario en el sistema
        login(request, usuario)
        return redirect('app:decano')
    else:
        return render(request,'app/login.html')
        # Retorna un mens

def decano_view(request):
    return render(request, 'app/decano.html')


def estudiante_view(request):
    return render(request, 'app/estudiante.html')


# __________________________________________________________

def crearEstudiante_view(request):
    return render(request, 'app/crearEstudiante.html')


def form_crearEstudiante_view(request):
    return render(request, 'app/crearEstudiante.html')

def crearEstudiante_post(request):
    
    nombre = request.POST['nombre']
    apellido = request.POST['apellido']
    semestre = request.POST['semestre']
    email = request.POST['email']
    documento = request.POST['documento']

    id_usuario=request.user.id

    try:
        U = User()
        U.first_name= nombre
        U.last_name = apellido
        U.email = email
        U.username = email
        U.set_password (documento)
        U.save()

        estudiante=Estudiante()

        estudiante.semestreActual=semestre
        estudiante.user_id=U.id
        estudiante.facultad_id=1
        estudiante.documento=documento
        estudiante.save()
        return redirect('app:listadoEstudiantes')
    except:
        veri=True
        return redirect('crearEstudiante_post')  


def listadoEstudiantes_view(request):
    # usuario = request.user.id
    estudiante = Estudiante.objects.all()
    contexto = {'lista':estudiante}
    return render(request, 'app/listadoEstudiantes.html', contexto)


# __________________________________________________________


def crearVotacion_view(request):
    tipovotacion= TipoVotacion.objects.all()    
    contexto={'TipoVotacion':tipovotacion} 
    return render(request, 'app/crearVotacion.html', contexto)

def form_crearVotacion_view(request):
    return render(request, 'app/crearVotacion.html')

def crearVotacion_post(request):
    
    nombre = request.POST['nombreVotacion']
    fechainicio = request.POST['fechaInicio']
    fechafin = request.POST['fechaFinalizacion']
    ciclo = request.POST['ciclo']
    estado = request.POST['estado']
    
    
    id_usuario=request.user.id
    V = Votacion()
    V.nombre= nombre
    V.fechaInicio = fechainicio
    V.fechaFinal = fechafin
    V.estado_id=6
    V.facultad_id=1
    V.tipo_id=estado
    V.save()

    return redirect('app:listadoVotaciones')

def listadoVotaciones_view(request):
    votacion= Votacion.objects.all()
    contexto = {'Votacion':votacion}
    return render(request, 'app/listadoVotaciones.html', contexto)



# __________________________________________
def consultlaVotacionesRepre_view(request):
    votacion= Votacion.objects.all()
    contexto = {'Votacion':votacion}
    return render(request, 'app/consultlaVotacionesRepre.html',contexto)

def postularCandidato_view(request):
    votacion= Votacion.objects.all()
    contexto = {'Votacion':votacion}
    return render(request, 'app/postularCandidato.html',contexto)

def form_postularCandidato_view(request):
    nombre = request.POST['estudiante_id']
    return render(request, 'app/postularCandidato.html')



def consultarVotacionE_view(request):
    votacion= Votacion.objects.all()
    contexto = {'Votacion':votacion}
    return render(request, 'app/consultarVotacionE.html', contexto)

def votar_repre_fa_view(request):
    return render(request, 'app/votar_repre_fa.html')

def votar_repre_se_view(request):
    return render(request, 'app/votar_repre_se.html')

def listadodevotaciones_view(request):
    return render(request, 'app/listadodevotaciones.html')

def consultarvotacion_view(request):
    return render(request, 'app/consultarvotacion.html')

def postularse_view(request):
    return render(request, 'app/postularse.html')

def consultavoto_view(request):
    return render(request, 'app/consultavoto.html')
