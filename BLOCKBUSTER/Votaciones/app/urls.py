	
from django.urls import path 
from . import views 
 
app_name = 'app' 
urlpatterns = [
    path('', views.index, name='index'),
    path('categorias/', views.categorias_view, name= 'categorias'), 
    path('categoria/<int:id>', views.categoria_view, name= 'categoria'), 
    path('peliculas/', views.peliculas_view, name= 'peliculas'), 
    path('acerca/', views.acerca_view, name='acerca'),
    
    #formulario para categoria
    path('categorias/crear/', views.form_crear_categoria_view, name='form_crear_categoria_view'),
    path('categorias/crear_post/', views.crear_categoria_post, name='crear_categoria_post'),
]
