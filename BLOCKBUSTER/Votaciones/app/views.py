
from pickle import TRUE
import re
from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Categoria, Pelicula
 
def index(request):
    return HttpResponse("Hola, mundo")

def categorias_view(request):
    #obtiene toda las categoras
    lista = Categoria.objects.all()
    #crear contexto
    contexto= {
        'categorias': lista
    }
        
    return render(request, 'app/categorias.html', contexto)

def categoria_view(request, id):

    contexto = {
        'actor': 'Tom Hanks',
        'edad': '64',
        'esJubilado':False,
        'filmografia': [
            {'Titulo': 'Naufrago', 'year': 2000},
            {'Titulo': 'Forrest Gump', 'year': 1994},
            {'Titulo': 'Codigo DaVinci', 'year': 2006},
            {'Titulo': 'toy Story', 'year': 1995},
        ]
    }
    return render(request, 'app/categoria.html', contexto)

def form_crear_categoria_view(request):
    return render(request, 'app/crearCategoria.html')

def crear_categoria_post(request):
    nombre = request.POST['nombre']
    descripcion = request.POST['descripcion']

    c = Categoria()
    c.nombre = nombre
    c.descripcion = descripcion

    c.save()

    return redirect('app:categorias')

def peliculas_view(request):
    return render(request, "app/peliculas.html")

def acerca_view(request):
    return render(request, 'app/acerca.html')

