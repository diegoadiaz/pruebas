	
from django.urls import path 
from . import views 
 
app_name = 'app' 
urlpatterns = [
    path('', views.index, name='index'),

    path('login/', views.login_view, name='login'),
    path('entrar/', views.entrar, name='entrar'),
    path('decano/', views.decano_view, name='decano'),
    path('estudiante/', views.estudiante_view, name='estudiante'),

    path('crearEstudiante/', views.crearEstudiante_view, name='crearEstudiante'),
    path('crearEstudiante_crear/', views.form_crearEstudiante_view, name='form_crearEstudiante_view'),
    path('crearEstudiante_post/', views.crearEstudiante_post, name='crearEstudiante_post'),
    path('listadoEstudiantes/', views.listadoEstudiantes_view, name='listadoEstudiantes'),
    
    
    path('crearVotacion_crear/', views.form_crearVotacion_view, name='form_crearVotacion_view'),
    path('crearVotacion_post/', views.crearVotacion_post, name='crearVotacion_post'),

    path('crearVotacion/', views.crearVotacion_view, name='crearVotacion'),
    path('listadoVotaciones/', views.listadoVotaciones_view, name='listadoVotaciones'),




    
    path('consultlaVotacionesRepre/', views.consultlaVotacionesRepre_view, name='consultlaVotacionesRepre'),
    path('postularCandidato/', views.postularCandidato_view, name='postularCandidato'),
    path('consultarVotacionE/', views.consultarVotacionE_view, name='consultarVotacionE'),
    path('votar_repre_fa/', views.votar_repre_fa_view, name='votar_repre_fa'),
    path('votar_repre_se/', views.votar_repre_se_view, name='votar_repre_se'),
    path('listadodevotaciones/', views.listadodevotaciones_view, name='listadodevotaciones'),
    path('consultarvotacion/', views.consultarvotacion_view, name='consultarvotacion'),
    path('postularse/', views.postularse_view, name='postularse'),
    path('consultavoto/', views.consultavoto_view, name='consultavoto'),
]